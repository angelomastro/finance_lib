Reusable back-end library for a financial application. 

It allows to:
* create an asset (position, security)
* price it, as of today or as of past date, or get a time series from yahoo
* create a portfolio
* add, remove, edit assets in portfolio
* price a portfolio
* a cache class called market data store contains all securities priced at yahoo. It is based on the observer pattern, so all portfolios pointing to its securities get the most up to date data. Whenever a security or a portfolio is refreshed, the relative securities in market data store are re requested to yahoo.

Testing

* unit tests provided 
* run  or view  ./mainApp.py to get an idea of how to use the Api 